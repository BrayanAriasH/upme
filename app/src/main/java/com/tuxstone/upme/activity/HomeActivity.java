package com.tuxstone.upme.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.squareup.picasso.Picasso;
import com.tuxstone.upme.R;
import com.tuxstone.upme.SplashActivity;
import com.tuxstone.upme.adapter.FuelAdapter;
import com.tuxstone.upme.database.CityDatabase;
import com.tuxstone.upme.database.DatabaseManager;
import com.tuxstone.upme.database.EdsDatabase;
import com.tuxstone.upme.database.SyncDatabase;
import com.tuxstone.upme.database.UserDatabase;
import com.tuxstone.upme.model.City;
import com.tuxstone.upme.model.Eds;
import com.tuxstone.upme.model.Fuel;
import com.tuxstone.upme.model.Sync;
import com.tuxstone.upme.model.UpmeImage;
import com.tuxstone.upme.model.User;
import com.tuxstone.upme.util.FileManager;
import com.tuxstone.upme.util.FuelUtil;
import com.tuxstone.upme.util.ModelUtil;
import com.tuxstone.upme.volley.CustomStringRequest;
import com.tuxstone.upme.volley.VolleyManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = HomeActivity.class.getName();
    private static final int PLACE_PICKER_REQUEST = 1;
    public static final int WRITE_PERMISSION = 100;
    public static final int LOCATION_PERMISSION = 200;
    private static final int TAKE_A_PICTURE = 500;
    private static final int TAKE_A_PICTURE_AD = 550;

    private UpmeImage adImage;
    private UpmeImage fuelImage;
    private FuelAdapter adapter;
    private TextView tvLocation;
    private boolean isInOperation;
    private List<Eds> edsList;
    private ProgressDialog dialogEds;
    private Place place;
    private Eds eds;
    private Toolbar toolbar;
    private String observations;
    private Uri photoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setIcon(R.drawable.ic_home);
        TextView tvCity = (TextView) findViewById(R.id.tvCity);
        City city = getIntent().getParcelableExtra("cityKey");
        if (city != null) {
            tvCity.setText(city.getName());
        }
        DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
        edsList = EdsDatabase.getEdsList(manager);
        Toolbar toolbarElements = (Toolbar) findViewById(R.id.toolbarElements);
        toolbarElements.setTitle(R.string.txt_fuel_list);
        toolbarElements.setSubtitle("0 elementos agregados");
        Toolbar toolbarObservations = (Toolbar) findViewById(R.id.toolbarObservations);
        toolbarObservations.setTitle(R.string.txt_observations);
        toolbarElements.inflateMenu(R.menu.menu_card);
        toolbarElements.getMenu().getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                adapter.setElements(loadDummy());
                adapter.notifyDataSetChanged();
                updateToolbar(0);
                return true;
            }
        });
        final RecyclerView rvElements = (RecyclerView) findViewById(R.id.rvElements);
        adapter = new FuelAdapter(loadDummy(), HomeActivity.this);
        rvElements.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        rvElements.setLayoutManager(layoutManager);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        //Boton tomar foto valla
        AppCompatButton btPhotoAd = (AppCompatButton) findViewById(R.id.btPhotoAd);
        if (btPhotoAd != null) {
            btPhotoAd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, WRITE_PERMISSION);
                        } else {
                            takeAPicture();
                        }
                    } else {
                        takeAPicture();
                    }
                }
            });
        }
        ImageView ivPhotoAd = (ImageView) findViewById(R.id.ivPhotoAd);
        if (ivPhotoAd != null) {
            ivPhotoAd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (adImage != null) {
                        showPhoto(adImage.getUri());
                    }
                }
            });
        }
        AppCompatButton btLocation = (AppCompatButton) findViewById(R.id.btLocation);
        if (btLocation != null) {
            btLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION
                            }, LOCATION_PERMISSION);
                        } else {
                            tvLocation.setText(R.string.txt_looking_for_location);
                            PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                            try {
                                startActivityForResult(placeBuilder.build(HomeActivity.this), PLACE_PICKER_REQUEST);
                            } catch (GooglePlayServicesRepairableException e) {
                                showSnackbar(e.getMessage());
                                Log.e(TAG, e.getMessage(), e);
                            } catch (GooglePlayServicesNotAvailableException e) {
                                showSnackbar(e.getMessage());
                                Log.e(TAG, e.getMessage(), e);
                            }

                        }
                    } else {
                        tvLocation.setText(R.string.txt_looking_for_location);
                        PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                        try {
                            Intent intent = placeBuilder.build(HomeActivity.this);
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                                intent.putExtra("primary_color", ContextCompat.getColor(HomeActivity.this, R.color.primary));
                                intent.putExtra("primary_color_dark", ContextCompat.getColor(HomeActivity.this, R.color.primary_dark));
                            }
                            startActivityForResult(intent, PLACE_PICKER_REQUEST);
                        } catch (GooglePlayServicesRepairableException e) {
                            showSnackbar(e.getMessage());
                            Log.e(TAG, e.getMessage(), e);
                        } catch (GooglePlayServicesNotAvailableException e) {
                            showSnackbar(e.getMessage());
                            Log.e(TAG, e.getMessage(), e);
                        }
                    }
                }
            });
        }
        AppCompatImageButton btSearch = (AppCompatImageButton) findViewById(R.id.btSearch);
        if (btSearch != null) {
            final TextView tvAddress = (TextView) findViewById(R.id.tvAddress);
            btSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadDialogEdsList();
                }
            });
            btSearch.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    eds = null;
                    tvAddress.setText(R.string.txt_no_eds_selected);
                    AppCompatEditText etStation = (AppCompatEditText) findViewById(R.id.etStation);
                    etStation.setText("");
                    return true;
                }
            });
        }
        AppCompatButton btSave = (AppCompatButton) findViewById(R.id.btSaveOnline);
        if (btSave != null) {
            btSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (validateForm()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                            builder.setTitle(getString(R.string.txt_send_data));
                            builder.setMessage(getString(R.string.txt_do_you_send_data));
                            builder.setCancelable(false);
                            builder.setNegativeButton(getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    saveData(false);
                                }
                            });
                            builder.show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
                                .show();
                    }
                }
            });
        }

        AppCompatButton btSaveLocal = (AppCompatButton) findViewById(R.id.btSaveLocal);
        if (btSaveLocal != null) {
            btSaveLocal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validateForm()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                        builder.setTitle("Guardar Datos");
                        builder.setMessage("¿Desea almacenar los datos recolectados?");
                        builder.setCancelable(false);
                        builder.setNegativeButton(getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                saveData(true);
                            }
                        });
                        builder.show();
                    }
                }
            });
        }

        AppCompatCheckBox cbStation = (AppCompatCheckBox) findViewById(R.id.cbStation);

        if (cbStation != null) {
            cbStation.setChecked(true);
            isInOperation = true;
            cbStation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    isInOperation = b;
                    if (!b) {
                        adapter.setElements(loadDummy());
                        adapter.notifyDataSetChanged();
                        updateToolbar(0);
                    }
                }
            });
        }
    }

    private void loadDialogEdsList() {
        final AppCompatEditText etStation = (AppCompatEditText) findViewById(R.id.etStation);
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        if (etStation.getText().toString().isEmpty()) {
            builder.setTitle(getString(R.string.txt_select_station));
            builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            final ArrayAdapter<Eds> adapter = new ArrayAdapter<Eds>(getBaseContext(),
                    R.layout.row_eds,
                    edsList);
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    TextView tvAddress = (TextView) findViewById(R.id.tvAddress);
                    eds = adapter.getItem(i);
                    etStation.setText(eds.toString());
                    tvAddress.setText(eds.getAddress());
                }
            });
        } else {
            List<Eds> result = ModelUtil.listEdsContains(edsList, etStation.getText().toString());
            if (result == null) {
                builder.setTitle(getString(R.string.txt_no_station));
                builder.setMessage(getString(R.string.txt_msg_no_station));
                builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                etStation.setText("");
            } else {
                builder.setTitle(getString(R.string.txt_select_station));
                builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                final ArrayAdapter<Eds> adapter = new ArrayAdapter<Eds>(getBaseContext(),
                        R.layout.row_eds,
                        result);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TextView tvAddress = (TextView) findViewById(R.id.tvAddress);
                        eds = adapter.getItem(i);
                        etStation.setText(eds.toString());
                        tvAddress.setText(eds.getAddress());
                    }
                });
            }
        }
        builder.show();
    }

    private void invalidSesion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.txt_sesion_invalid));
        builder.setMessage(getString(R.string.txt_sesion_invalid_msg));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                logout();
            }
        });
        builder.show();
    }

    private List<Fuel> loadDummy() {
        List<Fuel> elements = new ArrayList<>();
        Fuel fuel = new Fuel();
        fuel.setId(-1);
        fuel.setName("ACMP");
        fuel.setValue1(0);
        fuel.setValue2(0);
        elements.add(fuel);
        return elements;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.itLogout:
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setIcon(R.drawable.ic_logout_color);
                builder.setTitle(R.string.txt_logout);
                builder.setMessage(getString(R.string.txt_logout_msg));
                builder.setCancelable(false);
                builder.setNegativeButton(getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        logout();
                    }
                });
                builder.show();
                return true;
            case R.id.itSync:
                DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
                User user = getIntent().getParcelableExtra("userKey");
                List<Sync> syncs = SyncDatabase.getSyncList(manager);
                if (syncs != null && syncs.size() > 0) {
                    Intent intent = new Intent(getBaseContext(), SyncActivity.class);
                    intent.putExtra("userKey", user);
                    startActivity(intent);
                } else {
                    showSnackbar(getString(R.string.txt_no_data_saved));
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {
        DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
        UserDatabase.deleteUser(manager);
        CityDatabase.deleteCity(manager);
        Intent intent = new Intent(getBaseContext(), SplashActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == WRITE_PERMISSION) {
            if (grantResults.length == 2 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                showSnackbar("Permisos concedidos, vuelve a realizar la acción.");
            } else {
                showSnackbar("Se requieren los permisos para agregar elementos.");
            }
        } else if (requestCode == LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showSnackbar("Permisos concedidos, vuelve a realizar la acción.");
            } else {
                showSnackbar("Se requieren los permisos para agregar la ubicación.");
            }
        }
    }

    public void showSnackbar(String message) {
        CoordinatorLayout rootLayout = (CoordinatorLayout) findViewById(R.id.rootLayout);
        Snackbar.make(rootLayout, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.txt_ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        return;
                    }
                }).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case PLACE_PICKER_REQUEST:
                    if (resultCode == RESULT_OK) {
                        place = PlacePicker.getPlace(this, data);
                        tvLocation.setText(place.getLatLng().toString());
                    }
                    break;
                case TAKE_A_PICTURE:
                    if (resultCode == RESULT_OK) {
                        try {
                            File photoFile = new File(photoUri.getPath());
                            FileManager.compressImage(photoUri, getBaseContext());
                            InputStream stream = getContentResolver().openInputStream(photoUri);
                            byte[] byteArray = new byte[stream.available()];
                            Log.e(TAG, "onActivityResult: stream.available " + stream.available());
                            stream.read(byteArray);
                            fuelImage = new UpmeImage();
                            fuelImage.setPath(photoFile.getAbsolutePath());
                            fuelImage.setBase64(Base64.encodeToString(byteArray, Base64.NO_WRAP));
                            fuelImage.setName(photoFile.getName());
                            fuelImage.setUri(photoUri);
                            int position = getIntent().getIntExtra("adapterPosition", -1);
                            if (position != -1) {
                                Fuel fuel = adapter.getElements().get(position);
                                fuel.setImage(fuelImage);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage(), e);
                            showSnackbar("Error codificando la imagen.");
                        }
                    }
                    break;
                case TAKE_A_PICTURE_AD:
                    if (resultCode == RESULT_OK) {
                        adImage = null;
                        ImageView ivPhotoAd = (ImageView) findViewById(R.id.ivPhotoAd);
                        try {
                            File photoFile = new File(photoUri.getPath());
                            FileManager.compressImage(photoUri, getBaseContext());
                            Picasso.with(getBaseContext())
                                    .load(photoUri)
                                    .resize(320, 240)
                                    .centerCrop()
                                    .into(ivPhotoAd);
                            InputStream stream = getContentResolver().openInputStream(photoUri);
                            byte[] byteArray = new byte[stream.available()];
                            Log.e(TAG, "onActivityResult: stream.available " + stream.available());
                            stream.read(byteArray);
                            adImage = new UpmeImage();
                            adImage.setUri(photoUri);
                            adImage.setPath(photoFile.getPath());
                            adImage.setBase64(Base64.encodeToString(byteArray, Base64.NO_WRAP));
                            adImage.setName(photoFile.getName());
                            showSnackbar("Toca la imagen para verla completa.");
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage(), e);
                            showSnackbar("Error codificando la imagen.");
                        }
                    }
                    break;
                default:
                    return;
            }
        } catch (Exception e) {
            Toast.makeText(getBaseContext()
                    , "Error al tomar la foto, Intente nuevamente."
                    , Toast.LENGTH_LONG)
                    .show();
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private void takeAPicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent actualIntent = this.getIntent();
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = FileManager.createImageFile();
            } catch (IOException e) {
                Toast.makeText(getBaseContext()
                        , "Error al tomar la foto, Intente nuevamente."
                        , Toast.LENGTH_LONG)
                        .show();
                Log.e(TAG, e.getMessage(), e);
            }
            if (photoFile != null) {
                photoUri = FileProvider.getUriForFile(getBaseContext(),
                        getApplicationContext().getPackageName() + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, TAKE_A_PICTURE_AD);


            }
        }
    }

    public void showPhoto(Uri uri) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }


    public void updateToolbar(int elements) {
        Toolbar toolbarElements = (Toolbar) findViewById(R.id.toolbarElements);
        if (elements == 1) {
            toolbarElements.setSubtitle(elements + " elemento agregado");
        } else {
            toolbarElements.setSubtitle(elements + " elementos agregados");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            File directoryUpme = FileManager.getImageDirectory();
            String[] files = directoryUpme.list();
            if (files != null && files.length > 0) {
                for (String file : files) {
                    if (file.endsWith(".jpg")) {
                        new File(directoryUpme, file).delete();
                    }
                }
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private void saveData(boolean local) {
        City city = getIntent().getParcelableExtra("cityKey");
        User user = getIntent().getParcelableExtra("userKey");
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String formatted = format.format(calendar.getTime());
        freeMemory();
        String url = getString(R.string.url_register);
        final Sync sync = new Sync();
        JSONObject inputObject = new JSONObject();
        try {
            inputObject.put("ID", 1);
            inputObject.put("token", "******************************");
            inputObject.put("USUARIO_SESION", "admin");
            inputObject.put("TIPO_CARGA", "1");
            inputObject.put("BANDERA", "ESSO");
            inputObject.put("DIRECCION", eds.getAddress());
            inputObject.put("SICOM", Integer.parseInt(eds.getCode()));
            sync.setUser(user.getName());
            inputObject.put("CODIGO_MUNICIPIO", city.getId());
            sync.setCity(city.getId());
            inputObject.put("EDS", eds.getCode());
            sync.setEds(eds.getCode());
            inputObject.put("ANO", calendar.get(Calendar.YEAR));
            sync.setYear(calendar.get(Calendar.YEAR) + "");
            inputObject.put("MES", calendar.get(Calendar.MONTH) + 1);
            sync.setMonth((calendar.get(Calendar.MONTH) + 1) + "");
            inputObject.put("REGISTRO", 0);
            sync.setRegister("0");
            inputObject.put("FECHA", formatted);
            sync.setDate(formatted);
            inputObject.put("OPERATIVA", (isInOperation ? "s" : "n"));
            sync.setOperative(isInOperation);
            //"ACPM", 1
            Fuel fuel = FuelUtil.findFuelFromList(adapter.getElements(), 1);
            fuel = fuel == null ? createFuelDummy() : fuel;
            inputObject.put("ACPM_V", fuel.getValue1());
            inputObject.put("ACPM_S", fuel.getValue2());
            inputObject.put("FOTO_ACPM", fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            sync.setAcpmValue1(fuel.getValue1());
            sync.setAcpmValue2(fuel.getValue2());
            sync.setAcpmPhoto(fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            //"Gasolina Corriente", 2
            fuel = FuelUtil.findFuelFromList(adapter.getElements(), 2);
            fuel = fuel == null ? createFuelDummy() : fuel;
            inputObject.put("CORRIENTE_V", fuel.getValue1());
            inputObject.put("CORRIENTE_S", fuel.getValue2());
            inputObject.put("FOTO_CORRIENTE", fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            sync.setCurrentValue1(fuel.getValue1());
            sync.setCurrentValue2(fuel.getValue2());
            sync.setCurrentPhoto(fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            //"Gasolina Extra", 3
            fuel = FuelUtil.findFuelFromList(adapter.getElements(), 3);
            fuel = fuel == null ? createFuelDummy() : fuel;
            inputObject.put("EXTRA_V", fuel.getValue1());
            inputObject.put("EXTRA_S", fuel.getValue2());
            inputObject.put("FOTO_EXTRA", fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            sync.setExtraValue1(fuel.getValue1());
            sync.setExtraValue2(fuel.getValue2());
            sync.setExtraPhoto(fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            //"GNV", 4
            fuel = FuelUtil.findFuelFromList(adapter.getElements(), 4);
            fuel = fuel == null ? createFuelDummy() : fuel;
            inputObject.put("GNV_V", fuel.getValue1());
            inputObject.put("GNV_S", fuel.getValue2());
            inputObject.put("FOTO_GNV", fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            sync.setGnvValue1(fuel.getValue1());
            sync.setGnvValue2(fuel.getValue2());
            sync.setGnvPhoto(fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            //"Otro", 5
            fuel = FuelUtil.findFuelFromList(adapter.getElements(), 5);
            fuel = fuel == null ? createFuelDummy() : fuel;
            inputObject.put("OTRO_V", fuel.getValue1());
            inputObject.put("OTRO_S", fuel.getValue2());
            inputObject.put("FOTO_OTRO", fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            sync.setOtherValue1(fuel.getValue1());
            sync.setOtherValue2(fuel.getValue2());
            sync.setOtherPhoto(fuel.getImage() == null ? "" : fuel.getImage().getBase64());
            inputObject.put("COMBUSTIBLES", (adapter.getItemCount() - 1));
            sync.setFuelCount(adapter.getItemCount() - 1);
            inputObject.put("OBSERVACIONES", observations);
            sync.setObservations(observations);
            inputObject.put("FOTO_VALLA", (adImage == null ? "" : adImage.getBase64()));
            sync.setStationPhoto(adImage == null ? "" : adImage.getBase64());

        } catch (JSONException e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
                    .show();
            return;
        }
        if (SyncDatabase.getSync(DatabaseManager.getInstance(getBaseContext()), sync) != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setTitle("Error");
            builder.setMessage(getString(R.string.txt_data_is_saved));
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
            return;
        }
        if (local) {
            DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
            sync.setStatus(Sync.PENDING);
            SyncDatabase.addSync(sync, manager);
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setTitle(getString(R.string.txt_data_saved));
            builder.setMessage(getString(R.string.txt_data_in_sync_list));
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
            limpiarDatos();
            return;
        }
        dialogEds = ProgressDialog.show(HomeActivity.this,
                getString(R.string.txt_saving_data),
                getString(R.string.txt_saving_data_msg),
                true);
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
        CustomStringRequest request = new CustomStringRequest(Request.Method.POST,
                url,
                inputObject.toString(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        dialogEds.dismiss();
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        try {
                            String data = jsonObject.getString("result");
                            if (data.equals("-2")) {
                                invalidSesion();
                            } /*else if (data.equals("-1")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                                builder.setTitle(getString(R.string.err_saving_data));
                                builder.setMessage(getString(R.string.err_saving_data_msg));
                                builder.setCancelable(false);
                                builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.show();
                            }*/ else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                                builder.setTitle(getString(R.string.txt_register_saved));
                                //builder.setMessage(getString(R.string.txt_register_saved_msg) + " " + data);
                                builder.setMessage(getString(R.string.txt_register_saved_msg));
                                builder.setCancelable(false);
                                builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
                                        sync.setStatus(Sync.DONE);
                                        SyncDatabase.addSync(sync, manager);
                                        limpiarDatos();
                                    }
                                });
                                builder.show();
                            }
                        } catch (JSONException e) {
                            String messageError = getString(R.string.err_data_recieved_wrong);
                            Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        dialogEds.dismiss();
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        String messageError = getString(R.string.err_saving_data_msg);
                        if (volleyError instanceof TimeoutError) {
                            messageError = getString(R.string.er_timeout);
                        } else if (volleyError instanceof NetworkError) {
                            messageError = getString(R.string.er_no_internet);
                        } else if (volleyError instanceof ParseError) {
                            messageError = getString(R.string.er_parse);
                        } else if (volleyError instanceof ServerError) {
                            messageError = getString(R.string.er_server);
                        }
                        Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                    }
                });
        request.setRetryPolicy(new DefaultRetryPolicy(
                5 * 60 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(getBaseContext())
                .getRequestQueue()
                .add(request);
    }

    private void limpiarDatos() {
        AppCompatEditText etStation = (AppCompatEditText) findViewById(R.id.etStation);
        AppCompatCheckBox cbStation = (AppCompatCheckBox) findViewById(R.id.cbStation);
        AppCompatEditText etObservations = (AppCompatEditText) findViewById(R.id.etObservations);
        TextView tvAddress = (TextView) findViewById(R.id.tvAddress);
        ImageView ivPhotoAd = (ImageView) findViewById(R.id.ivPhotoAd);
        adapter.setElements(loadDummy());
        adapter.notifyDataSetChanged();
        updateToolbar(0);
        eds = null;
        place = null;
        observations = null;
        adImage = null;
        fuelImage = null;
        tvAddress.setText(R.string.txt_no_eds_selected);
        etStation.setText("");
        cbStation.setChecked(true);
        etObservations.setText("");
        ivPhotoAd.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.ic_no_photo));
    }

    public boolean isInOperation() {
        return isInOperation;
    }

    private Fuel createFuelDummy() {
        Fuel fuel = new Fuel();
        fuel.setValue1(0);
        fuel.setValue2(0);
        UpmeImage image = null;
        try {
            image = FileManager.createFuelNoPhoto(getBaseContext());
        } catch (IOException e) {
            Log.e(TAG, "createFuelDummy: " + e.getMessage(), e);
        }
        fuel.setImage(image);
        return fuel;
    }

    private void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    private boolean validateForm() {
        TextInputLayout tilStation = (TextInputLayout) findViewById(R.id.tilStation);
        tilStation.setError("");
        AppCompatEditText etStation = (AppCompatEditText) findViewById(R.id.etStation);
        AppCompatEditText etObservations = (AppCompatEditText) findViewById(R.id.etObservations);
        observations = etObservations.getText().toString();
        Fuel other = FuelUtil.findFuelFromList(adapter.getElements(), 5);
        if (other != null && observations.isEmpty()) {
            showSnackbar(getString(R.string.txt_err_other_observation));
            etObservations.requestFocus();
            return false;
        }
        observations = observations.isEmpty() ? getString(R.string.txt_without_observation) : observations;
        observations = observations.replace("\n", " ");
        if (eds == null) {
            showSnackbar(getString(R.string.err_select_eds));
            etStation.requestFocus();
            return false;
        } else if (isInOperation && adImage == null) {
            showSnackbar(getString(R.string.err_photo_ad));
            return false;
        } else if (adapter.getItemCount() < 2 && isInOperation) {
            showSnackbar(getString(R.string.err_select_fuel));
            return false;
        } else {
            return true;
        }
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }
}
