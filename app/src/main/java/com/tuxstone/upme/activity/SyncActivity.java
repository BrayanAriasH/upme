package com.tuxstone.upme.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.tuxstone.upme.R;
import com.tuxstone.upme.SplashActivity;
import com.tuxstone.upme.adapter.SyncAdapter;
import com.tuxstone.upme.database.CityDatabase;
import com.tuxstone.upme.database.DatabaseManager;
import com.tuxstone.upme.database.SyncDatabase;
import com.tuxstone.upme.database.UserDatabase;
import com.tuxstone.upme.model.Sync;
import com.tuxstone.upme.model.User;
import com.tuxstone.upme.volley.CustomStringRequest;
import com.tuxstone.upme.volley.VolleyManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SyncActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private SyncAdapter adapter;
    private boolean syncing;
    CustomStringRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sincronización");
        syncing = false;
        RecyclerView rvSync = (RecyclerView) findViewById(R.id.rvSync);
        if (rvSync != null) {
            DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
            List<Sync> syncs = SyncDatabase.getSyncList(manager);
            adapter = new SyncAdapter(syncs);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvSync.setAdapter(adapter);
            rvSync.setLayoutManager(layoutManager);
            rvSync.setItemAnimator(new DefaultItemAnimator());

        }
        FloatingActionButton fabSync = (FloatingActionButton) findViewById(R.id.fabSync);
        if (fabSync != null) {
            fabSync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sync();
                }
            });
        }

    }

    private void sync() {
        List<Sync> syncs = new ArrayList<>();
        syncing = true;
        for (Sync sync : adapter.getElements()) {
            if (sync.getStatus() != Sync.DONE) {
                syncs.add(sync);
            }
        }

        if (syncs != null && syncs.size() > 0) {
            for (final Sync sync : syncs) {
                sync.setStatus(Sync.SYNCING);
                adapter.notifyDataSetChanged();
                String url = getString(R.string.url_register);
                User user = getIntent().getParcelableExtra("userKey");
                JSONObject inputObject = new JSONObject();
                try {
                    inputObject.put("token", "******************************");
                    inputObject.put("USUARIO_SESION", user.getName());
                    inputObject.put("ID", 1);
                    inputObject.put("TIPO_CARGA", "1");
                    inputObject.put("BANDERA", "*");
                    inputObject.put("DIRECCION", "*");
                    inputObject.put("SICOM", Integer.parseInt(sync.getEds()));
                    inputObject.put("CIUDAD", sync.getCity());
                    inputObject.put("EDS", sync.getEds());
                    inputObject.put("ANO", sync.getYear());
                    inputObject.put("MES", sync.getMonth());
                    inputObject.put("REGISTRO", 0);
                    inputObject.put("FECHA", sync.getDate());
                    inputObject.put("OPERATIVA", (sync.isOperative() ? "s" : "n"));
                    //"ACPM", 1
                    inputObject.put("ACPM_V", sync.getAcpmValue1());
                    inputObject.put("ACPM_S", sync.getAcpmValue2());
                    inputObject.put("FOTO_ACPM", sync.getAcpmPhoto());
                    //"Gasolina Corriente", 2
                    inputObject.put("CORRIENTE_V", sync.getCurrentValue1());
                    inputObject.put("CORRIENTE_S", sync.getCurrentValue2());
                    inputObject.put("FOTO_CORRIENTE", sync.getCurrentPhoto());
                    //"Gasolina Extra", 3
                    inputObject.put("EXTRA_V", sync.getExtraValue1());
                    inputObject.put("EXTRA_S", sync.getExtraValue2());
                    inputObject.put("FOTO_EXTRA", sync.getExtraPhoto());
                    //"GNV", 4
                    inputObject.put("GNV_V", sync.getGnvValue1());
                    inputObject.put("GNV_S", sync.getGnvValue2());
                    inputObject.put("FOTO_GNV", sync.getGnvPhoto());
                    //"Otro", 5
                    inputObject.put("OTRO_V", sync.getOtherValue1());
                    inputObject.put("OTRO_S", sync.getOtherValue2());
                    inputObject.put("FOTO_OTRO", sync.getOtherPhoto());
                    inputObject.put("COMBUSTIBLES", sync.getFuelCount());
                    inputObject.put("OBSERVACIONES", sync.getObservations());
                    inputObject.put("FOTO_VALLA", sync.getStationPhoto());

                } catch (JSONException e) {
                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                request = new CustomStringRequest(Request.Method.POST,
                        url,
                        inputObject.toString(),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject jsonObject) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                                try {
                                    String data = jsonObject.getString("result");
                                    DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
                                    if (data.equals("-2")) {
                                        invalidSesion();
                                    }/* else if (data.equals("-1")) {
                                        Toast.makeText(getBaseContext(), getString(R.string.err_saving_data_msg), Toast.LENGTH_LONG)
                                                .show();
                                        sync.setStatus(Sync.DONE);
                                        SyncDatabase.updateSync(sync, manager);
                                        List<Sync> newList = SyncDatabase.getSyncList(manager);
                                        adapter.setElements(newList);
                                        adapter.notifyDataSetChanged();
                                        sync();
                                    }*/ else {
                                        sync.setStatus(Sync.DONE);
                                        SyncDatabase.updateSync(sync, manager);
                                        Toast.makeText(getBaseContext(), sync.getEds() + " se ha guardado.", Toast.LENGTH_LONG)
                                                .show();
                                        List<Sync> newList = SyncDatabase.getSyncList(manager);
                                        adapter.setElements(newList);
                                        adapter.notifyDataSetChanged();
                                        sync();
                                    }
                                } catch (JSONException e) {
                                    String messageError = getString(R.string.err_data_recieved_wrong);
                                    Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                                String messageError = getString(R.string.err_saving_data_msg);
                                if (volleyError instanceof TimeoutError) {
                                    messageError = getString(R.string.er_timeout);
                                } else if (volleyError instanceof NetworkError) {
                                    messageError = getString(R.string.er_no_internet);
                                } else if (volleyError instanceof ParseError) {
                                    messageError = getString(R.string.er_parse);
                                } else if (volleyError instanceof ServerError) {
                                    messageError = getString(R.string.er_server);
                                }
                                Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                            }
                        });
                request.setRetryPolicy(new DefaultRetryPolicy(
                        5*60*1000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyManager.getInstance(getBaseContext())
                        .getRequestQueue()
                        .add(request);
                Toast.makeText(getBaseContext(), "Sincronizando eds " + sync.getEds(), Toast.LENGTH_LONG)
                        .show();
            }
        } else {
            syncing = false;
            showSnackbar("No hay sincronizaciones pendientes.");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSnackbar(String message) {
        CoordinatorLayout rootLayout = (CoordinatorLayout) findViewById(R.id.rootLayout);
        Snackbar.make(rootLayout, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.txt_ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        return;
                    }
                }).show();
    }

    private void invalidSesion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SyncActivity.this);
        builder.setTitle(getString(R.string.txt_sesion_invalid));
        builder.setMessage(getString(R.string.txt_sesion_invalid_msg));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                logout();
            }
        });
        builder.show();
    }

    private void logout() {
        DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
        UserDatabase.deleteUser(manager);
        CityDatabase.deleteCity(manager);
        Intent intent = new Intent(getBaseContext(), SplashActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (syncing) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SyncActivity.this);
            builder.setTitle(getString(R.string.txt_sycing_progress));
            builder.setMessage(getString(R.string.txt_sycing_msg));
            builder.setCancelable(true);
            builder.setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    syncing = false;
                    request.cancel();
                    onBackPressed();
                }
            });
            builder.show();
        } else {
            super.onBackPressed();
        }
    }
}
