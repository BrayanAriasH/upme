package com.tuxstone.upme.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.tuxstone.upme.R;
import com.tuxstone.upme.SplashActivity;
import com.tuxstone.upme.adapter.CitySpinnerAdapter;
import com.tuxstone.upme.database.CityDatabase;
import com.tuxstone.upme.database.DatabaseManager;
import com.tuxstone.upme.database.EdsDatabase;
import com.tuxstone.upme.database.SyncDatabase;
import com.tuxstone.upme.database.UserDatabase;
import com.tuxstone.upme.model.City;
import com.tuxstone.upme.model.Eds;
import com.tuxstone.upme.model.User;
import com.tuxstone.upme.volley.CustomRequest;
import com.tuxstone.upme.volley.CustomStringRequest;
import com.tuxstone.upme.volley.VolleyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getName();
    Toolbar toolbar;
    String user;
    String password;
    City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        setContentView(R.layout.activity_login);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.txt_login);
        getSupportActionBar().setIcon(R.drawable.ic_login);
        /*Se carga listado de ciudades*/
        AppCompatSpinner spinner = (AppCompatSpinner) findViewById(R.id.spCity);
        CitySpinnerAdapter adapter = new CitySpinnerAdapter(getBaseContext(), getCities());
        spinner.setAdapter(adapter);
        /*Configuracion del boton de inicio de sesion*/
        Button btLogin = (Button) findViewById(R.id.btLogin);
        assert btLogin != null;
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()) {
                    login();
                }
            }
        });

    }

    private List<City> getCities() {
        DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
        return CityDatabase.getCityList(manager);
    }

    private void login() {
        final ProgressDialog progressDialog =
                ProgressDialog.show(LoginActivity.this,
                        getString(R.string.txt_login),
                        getString(R.string.txt_please_wait),
                        true);
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
        String url = getString(R.string.url_login);
        JSONObject in = new JSONObject();
        try {
            in.put("user", user.trim());
            in.put("pass", password.trim());
        } catch (JSONException e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG);
        }
        CustomStringRequest request = new CustomStringRequest(Request.Method.POST,
                url,
                in.toString(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        progressDialog.dismiss();
                        try {
                            //JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("resultado");
                            String[] dataUser = result.split("\\|");
                            if (result.startsWith("-1")) {
                                String messageError = getString(R.string.txt_wrong_user_pass);
                                showSnackbar(messageError);
                            } else if (dataUser.length >= 2) {
                                User user = new User();
                                user.setToken(dataUser[0]);
                                user.setName(dataUser[1]);
                                user.setCity(city.getId());
                                user.setLimitMin(0);
                                user.setLimitMax(50000);
                                ProgressDialog dialogEDS =
                                        ProgressDialog.show(LoginActivity.this,
                                                getString(R.string.txt_loading_eds_title),
                                                getString(R.string.txt_loading_eds_msg),
                                                true);
                                int currentOrientation = getResources().getConfiguration().orientation;
                                if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                                } else {
                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                                }
                                dialogEDS.show();
                                loadEds(user, city, dialogEDS);
                            } else {
                                String messageError = getString(R.string.txt_data_sesion_wrong);
                                Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG)
                                        .show();
                                recreate();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        progressDialog.dismiss();
                        String messageError = "Error cargando listado de ciudades.";
                        if (volleyError instanceof TimeoutError) {
                            messageError = getString(R.string.er_timeout);
                        } else if (volleyError instanceof NetworkError) {
                            messageError = getString(R.string.er_no_internet);
                        } else if (volleyError instanceof ParseError) {
                            messageError = getString(R.string.er_parse);
                        } else if (volleyError instanceof ServerError) {
                            messageError = getString(R.string.er_server);
                        }
                        Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                    }
                });
        VolleyManager.getInstance(getBaseContext())
                .getRequestQueue()
                .add(request);
    }

    private boolean validateForm() {
        TextInputLayout tilUser = (TextInputLayout) findViewById(R.id.tilUser);
        tilUser.setError("");
        AppCompatEditText etUser = (AppCompatEditText) findViewById(R.id.etUser);
        user = etUser.getText().toString();
        TextInputLayout tilPassword = (TextInputLayout) findViewById(R.id.tilPassword);
        tilPassword.setError("");
        AppCompatEditText etPassword = (AppCompatEditText) findViewById(R.id.etPassword);
        password = etPassword.getText().toString();
        AppCompatSpinner spinner = (AppCompatSpinner) findViewById(R.id.spCity);
        city = (City) spinner.getSelectedItem();
        if (user == null || user.isEmpty()) {
            tilUser.setError(getString(R.string.txt_type_user));
            etUser.requestFocus();
            return false;
        } else if (password == null || password.isEmpty()) {
            tilPassword.setError(getString(R.string.txt_type_password));
            etPassword.requestFocus();
            return false;
        } else if (city == null || city.getId().equals("-1")) {
            spinner.requestFocus();
            showSnackbar(getString(R.string.txt_select_city));
            return false;
        }
        return true;
    }

    private void showSnackbar(String message) {
        CoordinatorLayout rootLayout = (CoordinatorLayout) findViewById(R.id.rootLayout);
        Snackbar.make(rootLayout, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.txt_ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        return;
                    }
                }).show();
    }

    private void loadEds(final User user, final City city, final ProgressDialog dialogEDS) {
        String url = getString(R.string.url_eds);
        JSONObject in = new JSONObject();
        try {
            in.put("id", city.getId());
            in.put("token", user.getToken());
        } catch (JSONException e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG);
        }
        CustomRequest request = new CustomRequest(Request.Method.POST,
                url,
                in,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray array) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        dialogEDS.dismiss();
                        try {
                            if (array == null || array.length() == 0) {
                                invalidSesion();
                            } else {
                                DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
                                UserDatabase.deleteUser(manager);
                                EdsDatabase.deleteEds(manager);

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    Eds object = new Eds();
                                    object.setCode(jsonObject.getString("SICOM"));
                                    object.setName(jsonObject.getString("NOMBRE"));
                                    object.setAddress(jsonObject.getString("DIRECCION"));
                                    EdsDatabase.addEds(object, manager);
                                }
                                UserDatabase.addUser(user, manager);
                                SyncDatabase.deleteSync(manager, user);
                                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                                intent.putExtra("userKey", user);
                                intent.putExtra("cityKey", city);
                                startActivity(intent);
                                finish();
                            }
                        } catch (Exception e) {
                            String messageError = "Error cargando listado de EDS.";
                            Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        String messageError = "Error cargando listado de EDS.";
                        if (volleyError instanceof TimeoutError) {
                            messageError = getString(R.string.er_timeout);
                        } else if (volleyError instanceof NetworkError) {
                            messageError = getString(R.string.er_no_internet);
                        } else if (volleyError instanceof ParseError) {
                            messageError = getString(R.string.er_parse);
                        } else if (volleyError instanceof ServerError) {
                            messageError = getString(R.string.er_server);
                        }
                        Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                    }
                });
        VolleyManager.getInstance(getBaseContext())
                .getRequestQueue()
                .add(request);
    }

    private void invalidSesion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(getString(R.string.txt_sesion_invalid));
        builder.setMessage(getString(R.string.txt_sesion_invalid_msg));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                logout();
            }
        });
        builder.show();
    }

    private void logout() {
        DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
        UserDatabase.deleteUser(manager);
        CityDatabase.deleteCity(manager);
        Intent intent = new Intent(getBaseContext(), SplashActivity.class);
        finish();
        startActivity(intent);
    }

}
