package com.tuxstone.upme.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tuxstone.upme.R;
import com.tuxstone.upme.model.User;

import java.util.ArrayList;

public class UserDatabase {
    public static final String TAG = UserDatabase.class.getName();
    private static String userTableName = "User";

    public static ArrayList<User> getUserList(DatabaseManager manager) {
        ArrayList<User> users = new ArrayList<User>();
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_user);
            Cursor cursor = database.query(userTableName, columns, null, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    User user = new User();
                    user.setId(cursor.getInt(0));
                    user.setToken(cursor.getString(1));
                    user.setName(cursor.getString(2));
                    user.setCity(cursor.getString(3));
                    user.setLimitMin(cursor.getInt(4));
                    user.setLimitMax(cursor.getInt(5));
                    users.add(user);
                    cursor.moveToNext();
                }
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return users;
    }

    public static User getUserById(DatabaseManager manager, int id) {
        User user = new User();
        user.setId(-1);
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_user);
            String where = "ID = " + id;
            Cursor cursor = database.query(userTableName, columns, where, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                user = new User();
                user.setId(cursor.getInt(0));
                user.setToken(cursor.getString(1));
                user.setName(cursor.getString(2));
                user.setCity(cursor.getString(3));
                user.setLimitMin(cursor.getInt(4));
                user.setLimitMax(cursor.getInt(5));
            }
            if (database != null) {
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return user;
    }

    public static boolean addUser(User user, DatabaseManager manager) {
        boolean retorno = false;
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getWritableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_user);
            contentValues.put(columns[0], user.getId());
            contentValues.put(columns[1], user.getToken());
            contentValues.put(columns[2], user.getName());
            contentValues.put(columns[3], user.getCity());
            contentValues.put(columns[4], user.getLimitMin());
            contentValues.put(columns[5], user.getLimitMax());
            long rows = db.insert(userTableName, null, contentValues);
            db.close();
            retorno = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return retorno;
    }

    public static boolean updateUser(User user, DatabaseManager manager) {
        boolean mReturn = false;
        try {

            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_user);
            contentValues.put(columns[0], user.getId());
            contentValues.put(columns[1], user.getToken());
            contentValues.put(columns[2], user.getName());
            contentValues.put(columns[3], user.getCity());
            contentValues.put(columns[4], user.getLimitMin());
            contentValues.put(columns[5], user.getLimitMax());
            String where = "ID = '" + user.getId() + "'";
            long rows = db.update(userTableName, contentValues, where, null);
            db.close();
            mReturn = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }

    public static boolean deleteUser(DatabaseManager manager) {
        boolean mReturn = false;
        try {
            SQLiteDatabase db = manager.getReadableDatabase();
            db.delete(userTableName, null, null);
            db.close();
            mReturn = true;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }
}
