package com.tuxstone.upme.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tuxstone.upme.R;
import com.tuxstone.upme.model.City;

import java.util.ArrayList;

public class CityDatabase {
    public static final String TAG = CityDatabase.class.getName();
    private static String tableName = "City";

    public static ArrayList<City> getCityList(DatabaseManager manager) {
        ArrayList<City> cities = new ArrayList<City>();
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_city);
            Cursor cursor = database.query(tableName, columns, null, null, null, null, columns[1]);
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    City city = new City();
                    city.setId(cursor.getString(0));
                    city.setName(cursor.getString(1));
                    cities.add(city);
                    cursor.moveToNext();
                }
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return cities;
    }

    public static City getCityById(DatabaseManager manager, String id) {
        City city = new City();
        city.setId("-1");
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_city);
            String where = "ID = '" + id + "'";
            Cursor cursor = database.query(tableName, columns, where, null, null, null, columns[1]);
            if (cursor != null) {
                cursor.moveToFirst();
                city = new City();
                city.setId(cursor.getString(0));
                city.setName(cursor.getString(1));
            }
            if (database != null) {
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return city;
    }

    public static boolean addCity(City city, DatabaseManager manager) {
        boolean retorno = false;
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getWritableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_city);
            contentValues.put(columns[0], city.getId());
            contentValues.put(columns[1], city.getName());
            long rows = db.insert(tableName, null, contentValues);
            db.close();
            retorno = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return retorno;
    }

    public static boolean updateCity(City city, DatabaseManager manager) {
        boolean mReturn = false;
        try {

            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_city);
            contentValues.put(columns[0], city.getId());
            contentValues.put(columns[1], city.getName());
            String where = "ID = " + city.getId();
            long rows = db.update(tableName, contentValues, where, null);
            db.close();
            mReturn = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }

    public static boolean deleteCity(DatabaseManager manager) {
        boolean mReturn = false;
        try {
            SQLiteDatabase db = manager.getReadableDatabase();
            db.delete(tableName, null, null);
            db.close();
            mReturn = true;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }
}
