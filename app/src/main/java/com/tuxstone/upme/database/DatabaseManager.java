package com.tuxstone.upme.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tuxstone.upme.R;


public class DatabaseManager extends SQLiteOpenHelper {

    public static final String TAG = DatabaseManager.class.getName();
    private static final String dataBaseName = "dataCollector.db";
    private static final int dataBaseVersion = 2;
    private Context context;
    private static DatabaseManager dataBaseManager;

    private DatabaseManager(Context context) {
        super(context, DatabaseManager.dataBaseName, null, DatabaseManager.dataBaseVersion);
        this.context = context;
    }

    public static DatabaseManager getInstance(Context context) {
        if (dataBaseManager == null) {
            dataBaseManager = new DatabaseManager(context);
        }
        return dataBaseManager;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            String[] tables = this.context.getResources().getStringArray(R.array.createTables);
            for (String insertTable : tables) {
                sqLiteDatabase.execSQL(insertTable);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        try {
            Log.w(TAG, "Deleting database version " + i + " by version " + i2);
            String deleteCommand = this.context.getString(R.string.deleteCommand);
            String[] tables = this.context.getResources().getStringArray(R.array.tablesName);
            for (int j = 0; j < tables.length; j++) {
                String command = deleteCommand.replace("$tableName", tables[i]);
                sqLiteDatabase.execSQL(command);
            }
            onCreate(sqLiteDatabase);
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
    }

    public SQLiteDatabase open() {
        SQLiteDatabase database = null;
        try {
            database = this.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return database;
    }

    public Context getContext() {
        return context;
    }
}
