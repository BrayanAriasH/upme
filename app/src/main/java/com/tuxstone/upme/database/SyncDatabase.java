package com.tuxstone.upme.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tuxstone.upme.R;
import com.tuxstone.upme.model.Sync;
import com.tuxstone.upme.model.User;

import java.util.ArrayList;

public class SyncDatabase {
    public static final String TAG = SyncDatabase.class.getName();
    private static String syncTableName = "sync";

    public static ArrayList<Sync> getSyncList(DatabaseManager manager) {
        ArrayList<Sync> syncs = new ArrayList<Sync>();
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_sync);
            Cursor cursor = database.query(syncTableName, columns, null, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Sync sync = new Sync();
                    sync.setUser(cursor.getString(0));
                    sync.setCity(cursor.getString(1));
                    sync.setEds(cursor.getString(2));
                    sync.setYear(cursor.getString(3));
                    sync.setMonth(cursor.getString(4));
                    sync.setRegister(cursor.getString(5));
                    sync.setDate(cursor.getString(6));
                    sync.setOperative(cursor.getString(7).equals("1"));
                    sync.setAcpmValue1(cursor.getLong(8));
                    sync.setAcpmValue2(cursor.getLong(9));
                    sync.setAcpmPhoto(cursor.getString(10));

                    sync.setCurrentValue1(cursor.getLong(11));
                    sync.setCurrentValue2(cursor.getLong(12));
                    sync.setCurrentPhoto(cursor.getString(13));

                    sync.setExtraValue1(cursor.getLong(14));
                    sync.setExtraValue2(cursor.getLong(15));
                    sync.setExtraPhoto(cursor.getString(16));

                    sync.setGnvValue1(cursor.getLong(17));
                    sync.setGnvValue2(cursor.getLong(18));
                    sync.setGnvPhoto(cursor.getString(19));

                    sync.setOtherValue1(cursor.getLong(20));
                    sync.setOtherValue2(cursor.getLong(21));
                    sync.setOtherPhoto(cursor.getString(22));

                    sync.setFuelCount(cursor.getInt(23));
                    sync.setObservations(cursor.getString(24));
                    sync.setStationPhoto(cursor.getString(25));
                    sync.setStatus(cursor.getInt(26));
                    sync.setStatusInformation(cursor.getString(27));
                    syncs.add(sync);
                    cursor.moveToNext();
                }
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return syncs;
    }

    public static ArrayList<Sync> getSyncPendingList(DatabaseManager manager) {
        ArrayList<Sync> syncs = new ArrayList<>();
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_sync);
            String where = "STATUS <> 1";
            Cursor cursor = database.query(syncTableName, columns, null, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Sync sync = new Sync();
                    sync.setUser(cursor.getString(0));
                    sync.setCity(cursor.getString(1));
                    sync.setEds(cursor.getString(2));
                    sync.setYear(cursor.getString(3));
                    sync.setMonth(cursor.getString(4));
                    sync.setRegister(cursor.getString(5));
                    sync.setDate(cursor.getString(6));
                    sync.setOperative(cursor.getString(7).equals("1"));
                    sync.setAcpmValue1(cursor.getLong(8));
                    sync.setAcpmValue2(cursor.getLong(9));
                    sync.setAcpmPhoto(cursor.getString(10));

                    sync.setCurrentValue1(cursor.getLong(11));
                    sync.setCurrentValue2(cursor.getLong(12));
                    sync.setCurrentPhoto(cursor.getString(13));

                    sync.setExtraValue1(cursor.getLong(14));
                    sync.setExtraValue2(cursor.getLong(15));
                    sync.setExtraPhoto(cursor.getString(16));

                    sync.setGnvValue1(cursor.getLong(17));
                    sync.setGnvValue2(cursor.getLong(18));
                    sync.setGnvPhoto(cursor.getString(19));

                    sync.setOtherValue1(cursor.getLong(20));
                    sync.setOtherValue2(cursor.getLong(21));
                    sync.setOtherPhoto(cursor.getString(22));

                    sync.setFuelCount(cursor.getInt(23));
                    sync.setObservations(cursor.getString(24));
                    sync.setStationPhoto(cursor.getString(25));
                    sync.setStatus(cursor.getInt(26));
                    sync.setStatusInformation(cursor.getString(27));
                    syncs.add(sync);
                    cursor.moveToNext();
                }
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return syncs;
    }

    public static Sync getSync(DatabaseManager manager, Sync register) {
        Sync sync = null;
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String where = "USER = '" + register.getUser() + "' and "
                    + "CITY = '" + register.getCity() + "' and "
                    + "EDS = '" + register.getEds() + "' and "
                    + "YEAR = '" + register.getYear() + "' and "
                    + "MONTH = '" + register.getMonth() + "'";
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_sync);
            Cursor cursor = database.query(syncTableName, columns, where, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.getCount() == 0) {
                    return null;
                }
                sync = new Sync();
                sync.setUser(cursor.getString(0));
                sync.setCity(cursor.getString(1));
                sync.setEds(cursor.getString(2));
                sync.setYear(cursor.getString(3));
                sync.setMonth(cursor.getString(4));
                sync.setRegister(cursor.getString(5));
                sync.setDate(cursor.getString(6));
                sync.setOperative(cursor.getString(7).equals("1"));
                sync.setAcpmValue1(cursor.getLong(8));
                sync.setAcpmValue2(cursor.getLong(9));
                sync.setAcpmPhoto(cursor.getString(10));

                sync.setCurrentValue1(cursor.getLong(11));
                sync.setCurrentValue2(cursor.getLong(12));
                sync.setCurrentPhoto(cursor.getString(13));

                sync.setExtraValue1(cursor.getLong(14));
                sync.setExtraValue2(cursor.getLong(15));
                sync.setExtraPhoto(cursor.getString(16));

                sync.setGnvValue1(cursor.getLong(17));
                sync.setGnvValue2(cursor.getLong(18));
                sync.setGnvPhoto(cursor.getString(19));

                sync.setOtherValue1(cursor.getLong(20));
                sync.setOtherValue2(cursor.getLong(21));
                sync.setOtherPhoto(cursor.getString(22));

                sync.setFuelCount(cursor.getInt(23));
                sync.setObservations(cursor.getString(24));
                sync.setStationPhoto(cursor.getString(25));
                sync.setStatus(cursor.getInt(26));
                sync.setStatusInformation(cursor.getString(27));
                cursor.moveToNext();
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return sync;
    }

    public static boolean addSync(Sync sync, DatabaseManager manager) {
        boolean retorno = false;
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getWritableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_sync);
            contentValues.put(columns[0], sync.getUser());
            contentValues.put(columns[1], sync.getCity());
            contentValues.put(columns[2], sync.getEds());
            contentValues.put(columns[3], sync.getYear());
            contentValues.put(columns[4], sync.getMonth());
            contentValues.put(columns[5], sync.getRegister());
            contentValues.put(columns[6], sync.getDate());
            contentValues.put(columns[7], sync.isOperative() ? "1" : "0");
            contentValues.put(columns[8], sync.getAcpmValue1());
            contentValues.put(columns[9], sync.getAcpmValue2());
            contentValues.put(columns[10], sync.getAcpmPhoto());

            contentValues.put(columns[11], sync.getCurrentValue1());
            contentValues.put(columns[12], sync.getCurrentValue2());
            contentValues.put(columns[13], sync.getCurrentPhoto());

            contentValues.put(columns[14], sync.getExtraValue1());
            contentValues.put(columns[15], sync.getExtraValue2());
            contentValues.put(columns[16], sync.getExtraPhoto());

            contentValues.put(columns[17], sync.getGnvValue1());
            contentValues.put(columns[18], sync.getGnvValue2());
            contentValues.put(columns[19], sync.getGnvPhoto());

            contentValues.put(columns[20], sync.getOtherValue1());
            contentValues.put(columns[21], sync.getOtherValue2());
            contentValues.put(columns[22], sync.getOtherPhoto());

            contentValues.put(columns[23], sync.getFuelCount());
            contentValues.put(columns[24], sync.getObservations());
            contentValues.put(columns[25], sync.getStationPhoto());
            contentValues.put(columns[26], sync.getStatus());
            contentValues.put(columns[27], sync.getStatusInformation());
            long rows = db.insert(syncTableName, null, contentValues);
            db.close();
            retorno = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return retorno;
    }

    public static boolean updateSync(Sync sync, DatabaseManager manager) {
        boolean mReturn = false;
        try {
            String where = "USER = '" + sync.getUser() + "' and "
                    + "CITY = '" + sync.getCity() + "' and "
                    + "EDS = '" + sync.getEds() + "' and "
                    + "YEAR = '" + sync.getYear() + "' and "
                    + "MONTH = '" + sync.getMonth() + "'";
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_sync);
            contentValues.put(columns[0], sync.getUser());
            contentValues.put(columns[1], sync.getCity());
            contentValues.put(columns[2], sync.getEds());
            contentValues.put(columns[3], sync.getYear());
            contentValues.put(columns[4], sync.getMonth());
            contentValues.put(columns[5], sync.getRegister());
            contentValues.put(columns[6], sync.getDate());
            contentValues.put(columns[7], sync.isOperative() ? "1" : "0");
            contentValues.put(columns[8], sync.getAcpmValue1());
            contentValues.put(columns[9], sync.getAcpmValue2());
            contentValues.put(columns[10], sync.getAcpmPhoto());

            contentValues.put(columns[11], sync.getCurrentValue1());
            contentValues.put(columns[12], sync.getCurrentValue2());
            contentValues.put(columns[13], sync.getCurrentPhoto());

            contentValues.put(columns[14], sync.getExtraValue1());
            contentValues.put(columns[15], sync.getExtraValue2());
            contentValues.put(columns[16], sync.getExtraPhoto());

            contentValues.put(columns[17], sync.getGnvValue1());
            contentValues.put(columns[18], sync.getGnvValue2());
            contentValues.put(columns[19], sync.getGnvPhoto());

            contentValues.put(columns[20], sync.getOtherValue1());
            contentValues.put(columns[21], sync.getOtherValue2());
            contentValues.put(columns[22], sync.getOtherPhoto());

            contentValues.put(columns[23], sync.getFuelCount());
            contentValues.put(columns[24], sync.getObservations());
            contentValues.put(columns[25], sync.getStationPhoto());
            contentValues.put(columns[26], sync.getStatus());
            contentValues.put(columns[27], sync.getStatusInformation());
            long rows = db.update(syncTableName, contentValues, where, null);
            db.close();
            mReturn = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }


    public static boolean deleteSync(DatabaseManager manager, User user) {
        boolean mReturn = false;
        try {
            SQLiteDatabase db = manager.getReadableDatabase();
            String where = "USER <> '" + user.getName() + "'";
            db.delete(syncTableName, where, null);
            db.close();
            mReturn = true;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }
}
