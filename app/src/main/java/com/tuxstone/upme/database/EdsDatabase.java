package com.tuxstone.upme.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tuxstone.upme.R;
import com.tuxstone.upme.model.Eds;

import java.util.ArrayList;

public class EdsDatabase {
    public static final String TAG = EdsDatabase.class.getName();
    private static String edsTableName = "eds";

    public static ArrayList<Eds> getEdsList(DatabaseManager manager) {
        ArrayList<Eds> edses = new ArrayList<Eds>();
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_eds);
            Cursor cursor = database.query(edsTableName, columns, null, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Eds eds = new Eds();
                    eds.setCode(cursor.getString(0));
                    eds.setName(cursor.getString(1));
                    eds.setAddress(cursor.getString(2));
                    edses.add(eds);
                    cursor.moveToNext();
                }
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return edses;
    }

    public static Eds getEdsById(DatabaseManager manager, String id) {
        Eds eds = new Eds();
        eds.setCode("-1");
        try {
            SQLiteDatabase database = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_eds);
            String where = "CODE = '" + id + "'";
            Cursor cursor = database.query(edsTableName, columns, where, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                eds = new Eds();
                eds.setCode(cursor.getString(0));
                eds.setName(cursor.getString(1));
                eds.setAddress(cursor.getString(2));
            }
            if (database != null) {
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return eds;
    }

    public static boolean addEds(Eds eds, DatabaseManager manager) {
        boolean retorno = false;
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getWritableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_eds);
            contentValues.put(columns[0], eds.getCode());
            contentValues.put(columns[1], eds.getName());
            contentValues.put(columns[2], eds.getAddress());
            long rows = db.insert(edsTableName, null, contentValues);
            db.close();
            retorno = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return retorno;
    }

    public static boolean updateEds(Eds eds, DatabaseManager manager) {
        boolean mReturn = false;
        try {

            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = manager.getReadableDatabase();
            String[] columns = manager.getContext()
                    .getResources().getStringArray(R.array.columns_for_eds);
            contentValues.put(columns[0], eds.getCode());
            contentValues.put(columns[1], eds.getName());
            contentValues.put(columns[2], eds.getAddress());
            String where = "CODE = '" + eds.getCode() + "'";
            long rows = db.update(edsTableName, contentValues, where, null);
            db.close();
            mReturn = rows != 0 && rows != -1;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }

    public static boolean deleteEds(DatabaseManager manager) {
        boolean mReturn = false;
        try {
            SQLiteDatabase db = manager.getReadableDatabase();
            db.delete(edsTableName, null, null);
            db.close();
            mReturn = true;
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return mReturn;
    }
}
