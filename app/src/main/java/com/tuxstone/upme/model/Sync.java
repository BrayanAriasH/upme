package com.tuxstone.upme.model;

public class Sync {
    public static final int SYNCING = -1;
    public static final int PENDING = 0;
    public static final int DONE = 1;
    public static final int FAILED = 2;

    private String user;
    private String city;
    private String eds;
    private String year;
    private String month;
    private String register;
    private String date;
    private boolean operative;
    private long acpmValue1;
    private long acpmValue2;
    private String acpmPhoto;
    private long currentValue1;
    private long currentValue2;
    private String currentPhoto;
    private long extraValue1;
    private long extraValue2;
    private String extraPhoto;
    private long gnvValue1;
    private long gnvValue2;
    private String gnvPhoto;
    private long otherValue1;
    private long otherValue2;
    private String otherPhoto;
    private long fuelCount;
    private String observations;
    private String stationPhoto;
    private int status;
    private String statusInformation;


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEds() {
        return eds;
    }

    public void setEds(String eds) {
        this.eds = eds;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isOperative() {
        return operative;
    }

    public void setOperative(boolean operative) {
        this.operative = operative;
    }

    public long getAcpmValue1() {
        return acpmValue1;
    }

    public void setAcpmValue1(long acpmValue1) {
        this.acpmValue1 = acpmValue1;
    }

    public long getAcpmValue2() {
        return acpmValue2;
    }

    public void setAcpmValue2(long acpmValue2) {
        this.acpmValue2 = acpmValue2;
    }

    public String getAcpmPhoto() {
        return acpmPhoto;
    }

    public void setAcpmPhoto(String acpmPhoto) {
        this.acpmPhoto = acpmPhoto;
    }

    public long getCurrentValue1() {
        return currentValue1;
    }

    public void setCurrentValue1(long currentValue1) {
        this.currentValue1 = currentValue1;
    }

    public long getCurrentValue2() {
        return currentValue2;
    }

    public void setCurrentValue2(long currentValue2) {
        this.currentValue2 = currentValue2;
    }

    public String getCurrentPhoto() {
        return currentPhoto;
    }

    public void setCurrentPhoto(String currentPhoto) {
        this.currentPhoto = currentPhoto;
    }

    public long getExtraValue1() {
        return extraValue1;
    }

    public void setExtraValue1(long extraValue1) {
        this.extraValue1 = extraValue1;
    }

    public long getExtraValue2() {
        return extraValue2;
    }

    public void setExtraValue2(long extraValue2) {
        this.extraValue2 = extraValue2;
    }

    public String getExtraPhoto() {
        return extraPhoto;
    }

    public void setExtraPhoto(String extraPhoto) {
        this.extraPhoto = extraPhoto;
    }

    public long getGnvValue1() {
        return gnvValue1;
    }

    public void setGnvValue1(long gnvValue1) {
        this.gnvValue1 = gnvValue1;
    }

    public long getGnvValue2() {
        return gnvValue2;
    }

    public void setGnvValue2(long gnvValue2) {
        this.gnvValue2 = gnvValue2;
    }

    public String getGnvPhoto() {
        return gnvPhoto;
    }

    public void setGnvPhoto(String gnvPhoto) {
        this.gnvPhoto = gnvPhoto;
    }

    public long getOtherValue1() {
        return otherValue1;
    }

    public void setOtherValue1(long otherValue1) {
        this.otherValue1 = otherValue1;
    }

    public long getOtherValue2() {
        return otherValue2;
    }

    public void setOtherValue2(long otherValue2) {
        this.otherValue2 = otherValue2;
    }

    public String getOtherPhoto() {
        return otherPhoto;
    }

    public void setOtherPhoto(String otherPhoto) {
        this.otherPhoto = otherPhoto;
    }

    public long getFuelCount() {
        return fuelCount;
    }

    public void setFuelCount(long fuelCount) {
        this.fuelCount = fuelCount;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getStationPhoto() {
        return stationPhoto;
    }

    public void setStationPhoto(String stationPhoto) {
        this.stationPhoto = stationPhoto;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusInformation() {
        return statusInformation;
    }

    public void setStatusInformation(String statusInformation) {
        this.statusInformation = statusInformation;
    }
}
