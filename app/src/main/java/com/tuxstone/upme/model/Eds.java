package com.tuxstone.upme.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Eds implements Parcelable {
    private String code;
    private String branch;
    private String name;
    private String city;
    private String address;
    private String phone;

    public Eds() {
    }

    protected Eds(Parcel in) {
        code = in.readString();
        branch = in.readString();
        name = in.readString();
        city = in.readString();
        address = in.readString();
        phone = in.readString();
    }

    public static final Creator<Eds> CREATOR = new Creator<Eds>() {
        @Override
        public Eds createFromParcel(Parcel in) {
            return new Eds(in);
        }

        @Override
        public Eds[] newArray(int size) {
            return new Eds[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(code);
        parcel.writeString(branch);
        parcel.writeString(name);
        parcel.writeString(city);
        parcel.writeString(address);
        parcel.writeString(phone);
    }

    @Override
    public String toString() {
        return code + " - " + name;
    }
}
