package com.tuxstone.upme.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Element implements Parcelable {
    long id;
    String name;

    public Element() {
    }

    public Element(String name, long id) {
        this.name = name;
        this.id = id;
    }

    protected Element(Parcel in) {
        id = in.readLong();
        name = in.readString();
    }

    public static final Creator<Element> CREATOR = new Creator<Element>() {
        @Override
        public Element createFromParcel(Parcel in) {
            return new Element(in);
        }

        @Override
        public Element[] newArray(int size) {
            return new Element[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
    }
}
