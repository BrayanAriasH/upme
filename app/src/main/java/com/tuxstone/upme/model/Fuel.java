package com.tuxstone.upme.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Fuel implements Parcelable {
    int id;
    String name;
    UpmeImage image;
    long value1;
    long value2;

    public Fuel() {
        //Constructor vacio necesario
    }

    protected Fuel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readParcelable(UpmeImage.class.getClassLoader());
        value1 = in.readLong();
        value2 = in.readLong();
    }

    public static final Creator<Fuel> CREATOR = new Creator<Fuel>() {
        @Override
        public Fuel createFromParcel(Parcel in) {
            return new Fuel(in);
        }

        @Override
        public Fuel[] newArray(int size) {
            return new Fuel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UpmeImage getImage() {
        return image;
    }

    public void setImage(UpmeImage image) {
        this.image = image;
    }

    public long getValue1() {
        return value1;
    }

    public void setValue1(long value1) {
        this.value1 = value1;
    }

    public long getValue2() {
        return value2;
    }

    public void setValue2(long value2) {
        this.value2 = value2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeParcelable(image, i);
        parcel.writeLong(value1);
        parcel.writeLong(value2);
    }
}
