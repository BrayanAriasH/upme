package com.tuxstone.upme.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class UpmeImage implements Parcelable {
    private String path;
    private String base64;
    private String name;
    private Uri uri;

    public UpmeImage() {
    }

    protected UpmeImage(Parcel in) {
        path = in.readString();
        base64 = in.readString();
        name = in.readString();
        uri = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Creator<UpmeImage> CREATOR = new Creator<UpmeImage>() {
        @Override
        public UpmeImage createFromParcel(Parcel in) {
            return new UpmeImage(in);
        }

        @Override
        public UpmeImage[] newArray(int size) {
            return new UpmeImage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(path);
        parcel.writeString(base64);
        parcel.writeString(name);
        parcel.writeParcelable(uri, i);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }
}
