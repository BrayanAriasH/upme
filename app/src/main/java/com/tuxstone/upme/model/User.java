package com.tuxstone.upme.model;


import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private int id;
    private String token;
    private String name;
    private String city;
    private int limitMin;
    private int limitMax;

    public User() {
        //Necessary contructor.
    }

    protected User(Parcel in) {
        id = in.readInt();
        token = in.readString();
        name = in.readString();
        city = in.readString();
        limitMin = in.readInt();
        limitMax = in.readInt();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getLimitMin() {
        return limitMin;
    }

    public void setLimitMin(int limitMin) {
        this.limitMin = limitMin;
    }

    public int getLimitMax() {
        return limitMax;
    }

    public void setLimitMax(int limitMax) {
        this.limitMax = limitMax;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(token);
        parcel.writeString(name);
        parcel.writeString(city);
        parcel.writeInt(limitMin);
        parcel.writeInt(limitMax);
    }
}
