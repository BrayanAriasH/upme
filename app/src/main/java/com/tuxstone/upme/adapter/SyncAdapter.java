package com.tuxstone.upme.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tuxstone.upme.R;
import com.tuxstone.upme.database.CityDatabase;
import com.tuxstone.upme.database.DatabaseManager;
import com.tuxstone.upme.database.EdsDatabase;
import com.tuxstone.upme.model.City;
import com.tuxstone.upme.model.Eds;
import com.tuxstone.upme.model.Sync;

import java.util.List;

public class SyncAdapter extends RecyclerView.Adapter<SyncAdapter.Holder> {
    private List<Sync> elements;

    public SyncAdapter(List<Sync> elements) {
        this.elements = elements;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_sync, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Sync sync = elements.get(position);
        Context context = holder.itemView.getContext();
        DatabaseManager manager = DatabaseManager.getInstance(context);
        Eds eds = EdsDatabase.getEdsById(manager, sync.getEds());
        City city = CityDatabase.getCityById(manager, sync.getCity());
        holder.tvEds.setText(eds.toString());
        holder.tvCity.setText(city.getName());
        holder.tvDate.setText("Mes " + sync.getMonth() + " Año " + sync.getYear());
        holder.vSeparator.setBackgroundColor(ContextCompat.getColor(context, android.R.color.darker_gray));
        if (sync.getStatus() == Sync.DONE) {
            holder.ivLogo.setImageResource(R.drawable.ic_done);
        } else if (sync.getStatus() == Sync.FAILED) {
            holder.ivLogo.setImageResource(R.drawable.ic_failed);
        } else if (sync.getStatus() == Sync.SYNCING) {
            holder.ivLogo.setImageResource(R.drawable.ic_uploading);
            holder.vSeparator.setBackgroundColor(ContextCompat.getColor(context, R.color.syncing));
        }
    }

    @Override
    public int getItemCount() {
        return elements == null ? 0 : elements.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView ivLogo;
        TextView tvEds;
        TextView tvDate;
        TextView tvCity;
        View vSeparator;

        public Holder(View itemView) {
            super(itemView);
            ivLogo = (ImageView) itemView.findViewById(R.id.ivLogo);
            tvEds = (TextView) itemView.findViewById(R.id.tvEds);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvCity = (TextView) itemView.findViewById(R.id.tvCity);
            vSeparator = itemView.findViewById(R.id.vSeparator);
        }
    }

    public List<Sync> getElements() {
        return elements;
    }

    public void setElements(List<Sync> elements) {
        this.elements = elements;
    }
}
