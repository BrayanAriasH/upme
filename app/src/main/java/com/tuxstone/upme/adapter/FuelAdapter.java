package com.tuxstone.upme.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tuxstone.upme.R;
import com.tuxstone.upme.activity.HomeActivity;
import com.tuxstone.upme.model.Element;
import com.tuxstone.upme.model.Fuel;
import com.tuxstone.upme.model.User;
import com.tuxstone.upme.util.FileManager;
import com.tuxstone.upme.util.FuelUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FuelAdapter extends RecyclerView.Adapter<FuelAdapter.FuelViewHolder> {
    private static final String TAG = FuelAdapter.class.getName();
    private static final int TAKE_A_PICTURE = 500;
    List<Fuel> elements;

    HomeActivity activity;

    public FuelAdapter(List<Fuel> elements, HomeActivity activity) {
        this.elements = elements;
        this.activity = activity;
    }

    @Override
    public FuelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_fuel, parent, false);
        FuelViewHolder holder = new FuelViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FuelViewHolder holder, int position) {
        Fuel fuel = elements.get(position);
        if (fuel.getId() == -1) {
            holder.tvFuelName.setText("");
            holder.tvValue1.setText("Agregar combustible");
            holder.tvValue1.setGravity(Gravity.CENTER_HORIZONTAL);
            holder.ivFuelPhoto.setScaleType(ImageView.ScaleType.CENTER);
            holder.tvValue2.setText("");
        } else {
            if (fuel.getImage() != null && fuel.getImage().getUri() != null) {
                Picasso.with(activity.getBaseContext())
                        .load(fuel.getImage().getUri())
                        .resize(130, 70)
                        .centerCrop()
                        .into(holder.ivFuelPhoto);
            } else {
                holder.ivFuelPhoto.setScaleType(ImageView.ScaleType.CENTER);
                Picasso.with(activity.getBaseContext())
                        .load(R.drawable.ic_no_fuel_photo)
                        .into(holder.ivFuelPhoto);
            }
            holder.tvFuelName.setText(fuel.getName());
            holder.tvValue1.setText("Valla: $" + fuel.getValue1());
            holder.tvValue1.setGravity(Gravity.CENTER_HORIZONTAL);
            holder.tvValue2.setText("Surtidor: $" + fuel.getValue2());
            holder.tvValue2.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    @Override
    public int getItemCount() {
        return elements == null ? 0 : elements.size();
    }

    class FuelViewHolder extends RecyclerView.ViewHolder {
        TextView tvFuelName;
        ImageView ivFuelPhoto;
        TextView tvValue1;
        TextView tvValue2;

        public FuelViewHolder(final View itemView) {
            super(itemView);
            tvFuelName = (TextView) itemView.findViewById(R.id.tvFuelName);
            ivFuelPhoto = (ImageView) itemView.findViewById(R.id.ivFuelPhoto);
            tvValue1 = (TextView) itemView.findViewById(R.id.tvValue1);
            tvValue2 = (TextView) itemView.findViewById(R.id.tvValue2);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!activity.isInOperation()) {
                        return;
                    }
                    final Fuel fuel = elements.get(getAdapterPosition());
                    if (fuel.getId() == -1) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        final Context context = itemView.getContext();
                        LayoutInflater inflater = LayoutInflater.from(context);
                        final View popup = inflater.inflate(R.layout.popup_fuel, null, false);
                        final TextInputLayout tilFuelValue1 = (TextInputLayout)
                                popup.findViewById(R.id.tilFuelValue1);
                        final AppCompatEditText etFuelValue1 = (AppCompatEditText)
                                popup.findViewById(R.id.etFuelValue1);
                        final TextInputLayout tilFuelValue2 = (TextInputLayout)
                                popup.findViewById(R.id.tilFuelValue2);
                        final AppCompatEditText etFuelValue2 = (AppCompatEditText)
                                popup.findViewById(R.id.etFuelValue2);
                        final AppCompatSpinner spFuel = (AppCompatSpinner)
                                popup.findViewById(R.id.spFuel);
                        final AppCompatButton btTakeAPicture = (AppCompatButton)
                                popup.findViewById(R.id.btTakeAPicture);
                        btTakeAPicture.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                takeAPicture(getAdapterPosition());
                            }
                        });
                        spFuel.setAdapter(new ElementAdapter(context, loadElements()));
                        builder.setTitle(context.getString(R.string.txt_add_fuel));
                        builder.setView(popup);
                        final AlertDialog dialog;
                        AppCompatButton btCancel = (AppCompatButton) popup.findViewById(R.id.btCancel);
                        AppCompatButton btSave = (AppCompatButton) popup.findViewById(R.id.btSave);
                        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                            dialog = builder.show();
                            btCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });
                            btSave.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Element element = (Element) spFuel.getSelectedItem();
                                    tilFuelValue1.setError("");
                                    tilFuelValue2.setError("");
                                    if (element == null || element.getId() == -1) {
                                        Toast.makeText(context,
                                                "Seleccione un combustible",
                                                Toast.LENGTH_LONG).show();
                                    } else {
                                        if (dialog != null) {
                                            Fuel fuelCreate = FuelUtil.findFuelFromList(elements, (int) element.getId());
                                            if (fuelCreate == null) {
                                                fuelCreate = new Fuel();
                                                fuelCreate.setId((int) element.getId());
                                                fuelCreate.setName(element.getName());
                                                fuelCreate.setImage(fuel.getImage());
                                                fuelCreate.setValue1(Long.parseLong(etFuelValue1.getText().toString()));
                                                fuelCreate.setValue2(Long.parseLong(etFuelValue2.getText().toString()));
                                                elements.add(fuelCreate);
                                            } else {
                                                fuelCreate.setId((int) element.getId());
                                                fuelCreate.setName(element.getName());
                                                fuelCreate.setImage(fuel.getImage());
                                                fuelCreate.setValue1(Long.parseLong(etFuelValue1.getText().toString()));
                                                fuelCreate.setValue2(Long.parseLong(etFuelValue2.getText().toString()));
                                            }
                                            notifyDataSetChanged();
                                            activity.updateToolbar(elements.size() - 1);
                                            fuel.setImage(null);
                                            dialog.dismiss();
                                        }
                                    }
                                }
                            });
                        } else {
                            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED ||
                                    ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                                            != PackageManager.PERMISSION_GRANTED) {
                                activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, activity.WRITE_PERMISSION);
                            } else {
                                dialog = builder.show();
                                btCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                                btSave.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Element element = (Element) spFuel.getSelectedItem();
                                        tilFuelValue1.setError("");
                                        tilFuelValue2.setError("");
                                        if (!validateLimits(etFuelValue1, tilFuelValue1) || !validateLimits(etFuelValue2, tilFuelValue2)) {
                                            return;
                                        }
                                        if (element == null || element.getId() == -1) {
                                            Toast.makeText(context,
                                                    "Seleccione un combustible",
                                                    Toast.LENGTH_LONG).show();
                                        } else {
                                            if (dialog != null) {
                                                Fuel fuelCreate = FuelUtil.findFuelFromList(elements, (int) element.getId());
                                                if (fuelCreate == null) {
                                                    fuelCreate = new Fuel();
                                                    fuelCreate.setId((int) element.getId());
                                                    fuelCreate.setName(element.getName());
                                                    fuelCreate.setImage(fuel.getImage());
                                                    String fuelValue = etFuelValue1.getText().toString();
                                                    if (fuelValue.isEmpty()) {
                                                        fuelValue = "0";
                                                    }
                                                    fuelCreate.setValue1(Long.parseLong(fuelValue));
                                                    fuelValue = etFuelValue2.getText().toString();
                                                    if (fuelValue.isEmpty()) {
                                                        fuelValue = "0";
                                                    }
                                                    fuelCreate.setValue2(Long.parseLong(fuelValue));
                                                    if (fuelCreate.getImage() == null) {
                                                        try {
                                                            fuelCreate.setImage(FileManager.createFuelNoPhoto(activity.getBaseContext()));
                                                        } catch (IOException e) {
                                                            Log.e(TAG, "onClick: " + e.getMessage(), e);
                                                        }
                                                    }
                                                    elements.add(fuelCreate);
                                                } else {
                                                    fuelCreate.setId((int) element.getId());
                                                    fuelCreate.setName(element.getName());
                                                    fuelCreate.setImage(fuel.getImage());
                                                    String fuelValue = etFuelValue1.getText().toString();
                                                    if (fuelValue.isEmpty()) {
                                                        fuelValue = "0";
                                                    }
                                                    fuelCreate.setValue1(Long.parseLong(fuelValue));
                                                    fuelValue = etFuelValue2.getText().toString();
                                                    if (fuelValue.isEmpty()) {
                                                        fuelValue = "0";
                                                    }
                                                    fuelCreate.setValue2(Long.parseLong(fuelValue));
                                                    if (fuelCreate.getImage() == null) {
                                                        try {
                                                            fuelCreate.setImage(FileManager.createFuelNoPhoto(activity.getBaseContext()));
                                                        } catch (IOException e) {
                                                            Log.e(TAG, "onClick: " + e.getMessage(), e);
                                                        }
                                                    }
                                                }
                                                notifyDataSetChanged();
                                                fuel.setImage(null);
                                                activity.updateToolbar(elements.size() - 1);
                                                dialog.dismiss();
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
    }

    private void takeAPicture(int adapterPosition) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent actualIntent = activity.getIntent();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = FileManager.createImageFile();
            } catch (IOException e) {
                Toast.makeText(activity.getBaseContext()
                        , "Error al tomar la foto, Intente nuevamente."
                        , Toast.LENGTH_LONG)
                        .show();
                Log.e(TAG, e.getMessage(), e);
            }
            if (photoFile != null) {
                activity.setPhotoUri(FileProvider.getUriForFile(activity.getBaseContext(),
                        activity.getApplicationContext().getPackageName() + ".provider",
                        photoFile));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, activity.getPhotoUri());
                actualIntent
                        .putExtra("adapterPosition", adapterPosition);
                activity.startActivityForResult(takePictureIntent, TAKE_A_PICTURE);

            }
        }
    }

    private List<Element> loadElements() {
        List<Element> elements = new ArrayList<>();
        elements.add(new Element("Seleccione combustible", -1));
        elements.add(new Element("ACPM", 1));
        elements.add(new Element("Gasolina Corriente", 2));
        elements.add(new Element("Gasolina Extra", 3));
        elements.add(new Element("GNV", 4));
        elements.add(new Element("Otro", 5));
        return elements;
    }

    public List<Fuel> getElements() {
        return elements;
    }

    public void setElements(List<Fuel> elements) {
        this.elements = elements;
    }

    private boolean validateLimits(AppCompatEditText et, TextInputLayout til) {
        String data = et.getText().toString();
        User user = activity.getIntent().getParcelableExtra("userKey");

        if (!data.trim().isEmpty() && !data.trim().equals("0")) {
            int value = Integer.parseInt(data);
            if (value < user.getLimitMin() || value > user.getLimitMax()) {
                til.setError(activity.getString(R.string.txt_value_out_limits));
                String msg = activity.getString(R.string.txt_msg_limits);
                msg = msg.replace("$$", user.getLimitMin() + "");
                msg = msg.replace("##", user.getLimitMax() + "");
                Toast.makeText(activity.getBaseContext(),
                        msg,
                        Toast.LENGTH_LONG)
                        .show();
                return false;
            }
        }
        return true;
    }
}
