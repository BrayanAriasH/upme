package com.tuxstone.upme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tuxstone.upme.R;
import com.tuxstone.upme.model.Element;

import java.util.List;

public class ElementAdapter extends ArrayAdapter<Element> {
    List<Element> elements;

    public ElementAdapter(Context context, List<Element> objects) {
        super(context, R.layout.row_spinner, objects);
        elements = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.row_spinner, parent, false);
        TextView tvRowSpinner = (TextView) row.findViewById(R.id.tvRowSpinner);
        String city = elements.get(position).getName();
        tvRowSpinner.setText(city);
        return row;
    }
}