package com.tuxstone.upme.util;

import com.tuxstone.upme.model.Fuel;

import java.util.List;

/**
 * Created by brayan on 16/05/16.
 */
public class FuelUtil {

    public static Fuel findFuelFromList(List<Fuel> fuels, int id) {
        if (fuels != null) {
            for (Fuel fuel : fuels) {
                if (fuel.getId() == id) {
                    return fuel;
                }
            }
        }
        return null;
    }
}
