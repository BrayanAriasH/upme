package com.tuxstone.upme.util;

import com.tuxstone.upme.model.Eds;

import java.util.ArrayList;
import java.util.List;

public class ModelUtil {

    public static List<Eds> listEdsContains(List<Eds> original, String word) {
        if (word != null && original != null && original.size() > 0) {
            List<Eds> result = new ArrayList<>();
            for (Eds eds : original) {
                if (eds.toString().toLowerCase().contains(word.toLowerCase())) {
                    result.add(eds);
                }
            }
            if (result.size() == 0) {
                return null;
            } else {
                return result;
            }
        }
        return null;
    }
}
