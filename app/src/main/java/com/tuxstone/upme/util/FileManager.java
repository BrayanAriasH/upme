package com.tuxstone.upme.util;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.tuxstone.upme.R;
import com.tuxstone.upme.model.UpmeImage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class FileManager {
    public static final String TAG = FileManager.class.getName();

    public static String getDirectory() {
        File directory = new File(Environment.getExternalStorageDirectory() + "/DataCollector/");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory.getAbsolutePath();
    }

    public static File getImageDirectory() throws IOException {
        File directory = new File(Environment.getExternalStorageDirectory() + "/DataCollector/Images");
        if (!directory.exists()) {
            directory.mkdirs();
            File nomedia = new File(directory.getPath(), ".nomedia");
            nomedia.createNewFile();
        } else {
            File nomedia = new File(directory.getPath(), ".nomedia");
            if (!nomedia.exists()) {
                nomedia.createNewFile();
            }
        }
        return directory;
    }


    public static File createImageFile() throws IOException {
        String imageFileName = System.currentTimeMillis() + ".jpg";
        File storageDir = getImageDirectory();
        File image = new File(storageDir, imageFileName);
        image.createNewFile();
        return image;
    }


    public static String getPath(final Context context, final Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context,
                                       Uri uri,
                                       String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap scaledBitmap(File file) {
        if (file != null && file.exists()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        }
        return null;
    }


    public static void compressImage(Uri image, Context context) throws FileNotFoundException {
        InputStream inputStream = context.getContentResolver().openInputStream(image);
        Bitmap bm2 = BitmapFactory.decodeStream(inputStream);
        OutputStream stream = context.getContentResolver().openOutputStream(image);
        bm2.compress(Bitmap.CompressFormat.JPEG, 10, stream);
        bm2.recycle();
    }

    public static UpmeImage createFuelNoPhoto(Context context) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.no_image);
        int size = bitmap.getHeight() * bitmap.getRowBytes();
        ByteBuffer buffer = ByteBuffer.allocate(size);
        bitmap.copyPixelsToBuffer(buffer);
        InputStream stream = new ByteArrayInputStream(buffer.array());
        byte[] bytes = new byte[stream.available()];
        stream.read(bytes);
        UpmeImage fuelImage = new UpmeImage();
        fuelImage.setPath(" ");
        fuelImage.setBase64(Base64.encodeToString(bytes, Base64.NO_WRAP));
        fuelImage.setName("no_image.jpg");
        fuelImage.setUri(null);
        Log.e(TAG, "createFuelNoPhoto: " + fuelImage.getBase64().length());
        return fuelImage;
    }
}