package com.tuxstone.upme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.tuxstone.upme.activity.HomeActivity;
import com.tuxstone.upme.activity.LoginActivity;
import com.tuxstone.upme.database.CityDatabase;
import com.tuxstone.upme.database.DatabaseManager;
import com.tuxstone.upme.database.UserDatabase;
import com.tuxstone.upme.model.City;
import com.tuxstone.upme.model.User;
import com.tuxstone.upme.volley.CustomRequest;
import com.tuxstone.upme.volley.VolleyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        setContentView(R.layout.activity_splash);
        runActivity();
    }

    private void runActivity() {
        final DatabaseManager manager = DatabaseManager.getInstance(getBaseContext());
        List<User> users = UserDatabase.getUserList(manager);
        final List<City> cities = CityDatabase.getCityList(manager);
        Intent intent;
        if ((users == null || users.size() != 1)) {
            final TextView tvMessages = (TextView) findViewById(R.id.tvMessages);
            if (tvMessages != null) {
                tvMessages.setText(R.string.txt_loading_cities);
            }
            String url = getString(R.string.url_city);
            CustomRequest request = new CustomRequest(Request.Method.GET,
                    url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray array) {
                            try {
                                if (array.length() > 0) {
                                    CityDatabase.deleteCity(manager);
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject object = (JSONObject) array.get(i);
                                        City toSave = new City();
                                        toSave.setId(object.getString("MUNICIPIO"));
                                        toSave.setName(object.getString("NOMBRE"));
                                        CityDatabase.addCity(toSave, manager);
                                    }
                                    starLoginActivity();
                                } else {
                                    String messageError = getString(R.string.er_loading_cities);
                                    Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG)
                                            .show();
                                    runActivity();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
                                        .show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            String messageError = "Error cargando listado de ciudades.";
                            if (volleyError instanceof TimeoutError) {
                                messageError = getString(R.string.er_timeout);
                            } else if (volleyError instanceof NetworkError) {
                                messageError = getString(R.string.er_no_internet);
                            } else if (volleyError instanceof ParseError) {
                                messageError = getString(R.string.er_parse);
                            } else if (volleyError instanceof ServerError) {
                                messageError = getString(R.string.er_server);
                            }
                            Toast.makeText(getBaseContext(), messageError, Toast.LENGTH_LONG).show();
                            if (cities.size() > 0) {
                                starLoginActivity();
                            } else {
                                runActivity();
                            }
                        }
                    });
            VolleyManager.getInstance(getBaseContext())
                    .getRequestQueue()
                    .add(request);
        } else {
            intent = new Intent(getBaseContext(), HomeActivity.class);
            User user = users.get(0);
            City city = CityDatabase.getCityById(manager, user.getCity());
            intent.putExtra("userKey", user);
            intent.putExtra("cityKey", city);
            finish();
            startActivity(intent);
        }


    }

    private void starLoginActivity() {
        finish();
        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
        startActivity(intent);
    }
}
